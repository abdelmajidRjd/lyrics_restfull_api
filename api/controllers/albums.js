const Album = require('../models/album');
const mongoose = require('mongoose');


exports.create_album =  (req, res, next) => {

    const album =  new Album({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        subtitle: req.body.subtitle,
        albumImage: process.env.staging+req.file.path,
        styles: req.body.styles,
        genres: req.body.genres,
        country: req.body.country,
        releaseDate: req.body.releaseDate,
        songs: req.body.songs
    })

    album.save()
         .then(result => {
            res.status(200).json({
                message: 'The albums was added successfully',
                createdAlbum: {
                    id: result._id,
                    title: result.title,
                    subtitle: result.subtitle,
                    albumImage: result.albumImage,
                    styles: result.styles,
                    genres: result.genres,
                    country: result.country,
                    releaseDate: result.releaseDate,
                    songs: result.songs,
                    request: {
                            type: 'GET',
                            url: process.env.staging+'/albums/'+ result._id
                        }
                }

            })
         }).catch(err => {
            res.status(500).json({
                message : 'The albums could not be saved'
            })
         });
};


exports.get_all =  (req, res, next) => {

    Album.find()
         .populate('songs')
         .exec()
         .then(docs => {
             const response = {
                 count: docs.length,
                 albums: docs.map(doc => {
                     return {
                         id: doc._id,
                         releaseDate: doc.releaseDate,
                         genres:  doc.genres,
                         styles:  doc.styles,
                         albumImage: doc.albumImage,
                         title: doc.title,
                         subtitle: doc.subtitle,
                         country: doc.subtitle,
                         songs:  doc.songs,
                         request: {
                            type : 'GET',  
                            url: process.env.staging+'/albums/'+doc._id
                          }
                     }
                 }),
                 
             }
 
             res.status(200).json(
                 response
             )
         })
         .catch(error => {
             res.status(500).json({
                 message : "An error was occured - no albums was found"
             });
         });
 
 };

 exports.get_album_by_id =  (req, res, next) => {
    
    const albumId = req.params.id;

    Album.findById(albumId)
        .select('releaseDate title subtitle styles songs albumImage genre')
        .populate('songs', 'title label')
        .exec()
        .then(doc => {
            const response = {
                    id: doc._id,
                    releaseDate: doc.releaseDate,
                    genres:  doc.genres,
                    styles:  doc.styles,
                    albumImage: doc.albumImage,
                    title: doc.title,
                    subtitle: doc.subtitle,
                    country: doc.subtitle,
                    songs:  doc.songs,
                    request: {
                       type : 'GET',  
                       url: process.env.staging+'/albums'
                     }
             }
            
            
            res.status(200).json(response)

        }).catch(err => {
            res.status(500).json({
                message : 'the albums could not found'
            })
        })
    
};

exports.get_all_songs_for_album =  (req, res, next) => {
    
    const albumId = req.params.albumId;
    
    Album.findById(albumId)
        .populate('songs')
        .exec()
        .then(doc => {
            const response = {
            count: doc.songs.length,
            songs: doc.songs.map(song => {
                return {
                    releaseDate: song.releaseDate,
                    lyrics: song.lyrics,
                    id: song._id,
                    title: song.title,
                    label: song.label,
                    artist: song.artist,
                    styles: song.styles,
                    songImage: song.songImage
                }
            })
        }

        res.status(200).json(response);

        })
        .catch(err =>{
            res.status(500).json({
                message : 'The songs could not be saved'
            })
        })

};

exports.search_an_album = (req, res, next) => {
    
    const searchText = req.body.searchText; 

    Album.find({
        title: {$regex : "^"+ searchText }
       }).exec()
         .then(albums => {
            const response = {
                count: albums.length,
                albums: albums.map( album => {
                    return {
                    id: album._id,
                    title: album.title,
                    subtitle: album.subtitle,
                    albumImage: album.albumImage,
                    styles: album.styles,
                    genres: album.genres,
                    country: album.country,
                    releaseDate: album.releaseDate,
                    songs: album.songs,
                    request: {
                            type: 'GET',
                            url: process.env.staging+'/albums/'+ album._id
                        }
                    }
                })
            }

            res.status(200).json(response)
         })
         .catch(err => {
             res.status(500).json({
                 message: 'Not album title contain what you are looking for'+err
             })
         });
};

exports.remove_album  = (req, res, next) => {
    const albumId = req.params.albumId;

    Album.remove({
        _id: albumId
     }).exec()
       .then(doc => {

    res.status(200).json({
            message: 'album was deleted successfully',
            request: {
                type: 'GET',
                url: process.env.staging+'/albums'
            }
    })

    }).catch(err => {

        res.status(400).json({
            message : 'could not remove album'
        });
    });
}