const Lyric = require('../models/lyric');
const mongoose = require('mongoose');


exports.creat_lyric =  ( req, res, next )=> {

    const lyric = new Lyric({
        _id: new mongoose.Types.ObjectId(),
        language: req.body.language,
        body: req.body.body,
        youtube: req.body.youtube,
        label: req.body.label
    })

    lyric.save()
         .then( result => {
             console.log(result);
             res.status(201).json({
                message: 'Create lyric succesfully',
                createdLyric: {
                    id: result._id,
                    body: result.body,
                    language: result.language,
                    youtube: result.youtube,
                    request: {
                        type: 'POST',
                        url: process.env.staging+'/lyrics/'+ result._id
                    }
                }
             })
            }
         ).catch(err => {
            message: 'we could not create the lyric ... please read the doc'
         });
};


exports.get_all_lyrics =  (req, res, next)=> {
    
    Lyric.find()
         .select('language body youtube label _id')
         .exec()
         .then(docs => {
            const response  = {
                count: docs.length,
                lyrics: docs.map(doc => {
                   return {
                       id: doc._id,                       
                       language: doc.language,
                       body: doc.body,
                       label: doc.label,
                       request: {
                         type : 'GET',  
                         url: process.env.staging+'/lyrics/'+doc._id
                       }
                   } 
                })
            }
            res.status(200).json(
                response
            );
         }).catch( err => {
            res.status(500).json({
                message: err
            })
        });
     
    
};


exports.get_lyric_by_id =  (req, res, next) => {

    const id = req.params.lyricId;

    Lyric.findOne({
        _id: id 
    })
        .select('label youtube body')
        .exec()
        .then(doc =>{
            res.status(200)
               .json({
                   lyric: doc,
                   request: {
                       type: 'GET',
                       url: process.env.staging+"/lyrics"
                   }
               })
        
      }).catch(error =>{
        res.status(500).json({
            message : 'This lyric is not exist on database'+error
        })
      });

};
exports.remove_lyric =  (req, res, next) => {

    const id = req.params.lyricId;

    Lyric.remove({
        _id: id 
    })
        .exec()
        .then(doc =>{
            res.status(200)
               .json({
                   message: 'the lyric was successfully removed '+id,
                   request: {
                       type: 'GET',
                       url: process.env.staging+"/lyrics"
                   }
               })
        
      }).catch(error =>{
        res.status(500).json({
            message : 'This lyric is not exist on database'+error
        })
    });

};