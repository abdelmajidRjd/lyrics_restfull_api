const mongoose = require('mongoose');
const Song = require('../models/song');

exports.create_song =  (req, res, next) => {

    const song = new Song({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        releaseDate: req.body.releaseDate,
        songImage: process.env.staging+req.file.path,
        label: req.body.label,
        artist: req.body.artist,
        styles: req.body.styles,
        lyrics: req.body.lyrics
    });

    song.save().then( result => {

            res.status(201).json({
                message : 'song was added successfully',
                song: result,
                request: {
                    type : 'GET',  
                    url: process.env.staging+'/songs/'+result._id
                }  
            })
        }
    ).catch(error => {
        res.status(500).json({
            message: 'could not create new songs'
        })
    });
};

exports.get_all_songs = (req, res, next) => {
    Song
        .find()
        .select('_id title releaseDate songImage artist styles lyrics')
        .exec()
        .then(docs => {
            const response = {
                count : docs.length,
                songs: docs.map(doc => {

                    return {
                        _id: doc._id,
                        releaseDate: doc.releaseDate,
                        styles:  doc.styles,
                        songImage: doc.songImage,
                        title: doc.title,
                        artist: doc.artist,
                        label: doc.label,
                        lyrics: doc.lyrics,
                        request: {
                            type : 'GET',  
                            url: process.env.staging+'/songs/'+doc._id
                        }
                    } 
                })
            }
            res.status(200).json({response});

        }).catch(err => {
            res.status(500).json({
                message: 'Couldnt fetch all songs'
            })
        });  
};

exports.get_song_by_id = (req, res, next) => {
    const songId  = req.params.songId;
    Song.findById(songId)
        .select('title label songImage lyrics styles')
        .populate('lyrics')
        .exec()
        .then(result => {
          res.status(200).json({  
            song: result,
            request: {
                type : 'GET',  
                url: process.env.staging+'/songs/'
            } 
          })
      }).catch(error => {
          res.status(400).json({
              message: 'the songs is not exist on database'
          })
      });

};


exports.remove_song = (req, res, next) => {
    const songId  = req.params.songId;
    Song.remove({_id:songId})
        .exec()
        .then(result => {
          res.status(200).json({  
            message: "the item was successfully removed",
            request: {
                type : 'GET',  
                url: process.env.staging+'/songs/'
            } 
          })
      }).catch(error => {
          res.status(400).json({
              message: 'the songs is not exist on database'
          })
      });
};