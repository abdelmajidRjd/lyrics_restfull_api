const mongoose = require('mongoose');

const albumSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    releaseDate: { type: Number, default: 1 },
    genres:  { type: String },
    styles:  { type: String },
    title: { type: String, required: true },
    subtitle: { type: String, required: true },
    country: { type: String },
    albumImage: { type: String, required: true },
    songs:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Song', required: true }]
    
});

module.exports = mongoose.model('Album', albumSchema);