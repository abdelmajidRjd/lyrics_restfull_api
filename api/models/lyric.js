const mongoose = require('mongoose');

const lyricSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    youtube: { type: String, required: false},
    body: { type: String, required: true},
    language: { type: String, required: true},
    label: { type: String}
});

module.exports = mongoose.model('Lyric', lyricSchema);