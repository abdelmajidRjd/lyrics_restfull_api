const mongoose = require('mongoose');

const songSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    releaseDate: { type: Number, default: 2018 },
    styles:  { type: String},
    songImage: { type: String, require: true},
    title: { type: String, required: true},
    artist: { type: String},
    label: { type: String},
    lyrics: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Lyric', required: true }]
    
});

module.exports = mongoose.model('Song', songSchema);