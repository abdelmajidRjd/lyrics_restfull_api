const express = require('express');
const mongoose = require('mongoose');

const Album = require('../models/album');
const router = express.Router();

const albumController = require('../controllers/albums');

const multer = require('multer');

const storage = multer.diskStorage({

    destination: (req, file, cb) => {
        cb(null, './uploads/albums/');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString()+ file.originalname);
    } 

})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' ||  file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(new Error('could not save this kind of image'), false);
    }
}


const upload = multer({
    storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
})



router.get("/",  albumController.get_all);

router.get("/:id", albumController.get_album_by_id )

router.post("/", upload.single('albumImage') ,  albumController.create_album);

router.get("/:albumId/songs", albumController.get_all_songs_for_album);

router.post("/search", albumController.search_an_album);

router.delete('/:albumId', albumController.remove_album);

module.exports = router;

