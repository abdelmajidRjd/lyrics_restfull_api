const express = require('express');
const router = express.Router();




const lyricController = require('../controllers/lyrics');

router.get("/", lyricController.get_all_lyrics);

router.get('/:lyricId', lyricController.get_lyric_by_id);

router.delete('/:lyricId', lyricController.remove_lyric);

router.post("/", lyricController.creat_lyric);

module.exports = router;
