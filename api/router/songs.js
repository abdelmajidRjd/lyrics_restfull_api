const express = require('express');

const router = express.Router();

const songController = require('../controllers/songs');

const multer = require('multer');

const storage = multer.diskStorage({

    destination: (req, file, cb) => {
        cb(null, './uploads/songs/');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString()+ file.originalname);
    } 

})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' ||  file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(new Error('could not save this kind of image'), false);
    }
}


const upload = multer({
    storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
})




router.get("/",  songController.get_all_songs);

router.post("/", upload.single('songImage'), songController.create_song);

router.get("/:songId", songController.get_song_by_id);

router.delete("/:songId", songController.remove_song);

module.exports = router;