const express = require("express");
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();

const mongoose = require('mongoose');



mongoose.connect("mongodb+srv://abdelmajid:"+process.env.MONGO_ATLAS_PWD+"@shop-yr4yo.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true,
    ssl : true,
    replicaSet: 'Shop-shard-0',
    authSource : 'admin'
});
    
mongoose.Promise = global.Promise;  

const albumRoutes = require('./api/router/albums');
const songRoutes = require('./api/router/songs');
const lyricRoutes = require("./api/router/lyrics");

app.use('/uploads', express.static('uploads'))
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use((req, res, next) => {    
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Request-With, Content-Type, Accept, Authorization');
    
    
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, DELETE, POST, GET');
        return res.status(200).json({});
    }
    next();
});

app.use("/albums", albumRoutes);
app.use("/songs", songRoutes);
app.use("/lyrics", lyricRoutes);


app.use((req, res, next) => {
    const error = new Error("not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {

    res.status(error.status || 500);
    res.json({
        error : {
            message : error.message
        }
    });
});

module.exports = app;